#include <ESP8266WiFi.h>            
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>

#include <Bounce2.h>

int LED_RED = D7;
int BTN_ONE = D5;
int BTN_TWO = D6;

const char* WIFI_SSID = "";  // SSID to connect to
const char* WIFI_PW = "";  // wifi password
const String SERVER_ADDR = "http://192.168.1.100:8000/tables/";  // server URL

Bounce debouncerOne = Bounce();
Bounce debouncerTwo = Bounce();

void setup() {

  Serial.begin(115200);
  WiFi.begin(WIFI_SSID, WIFI_PW); //Connect to the WiFi network
  
  while(WiFi.status() != WL_CONNECTED) { //Wait for connection
    delay(500);
    Serial.println("Waiting to connect...");
  }
  
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP()); //Print the local IP to access the server
  
  pinMode(LED_RED, OUTPUT);
  
  debouncerOne.attach(BTN_ONE, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  debouncerOne.interval(25); // Use a debounce interval of 25 milliseconds
  debouncerTwo.attach(BTN_TWO, INPUT_PULLUP); // Attach the debouncer to a pin with INPUT_PULLUP mode
  debouncerTwo.interval(25); // Use a debounce interval of 25 milliseconds
}

void loop() {

  debouncerOne.update(); // Update the Bounce instance
  debouncerTwo.update(); // Update the Bounce instance

  if ( debouncerOne.fell() ) {
    digitalWrite(LED_RED, HIGH);
    postData(0);
    Serial.println("btn 0 pressed!");
    delay(500);
    digitalWrite(LED_RED, LOW);
  }

  if ( debouncerTwo.fell() ) {
    digitalWrite(LED_RED, HIGH);
    postData(1);
    Serial.println("btn 1 pressed!");
    delay(500);
    digitalWrite(LED_RED, LOW);
  }
  
}

void postData(int type) {
  HTTPClient http;    // Create a new HTTPClient object
 
  String postData;
  String label = "Table VIP 4";
  
  // Post Data
  postData = "label=" + label + "&type=" + type;
  
  http.begin(SERVER_ADDR);              //Specify request destination
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
 
  int httpCode = http.POST(postData);   //Send the request
  String payload = http.getString();    //Get the response payload
 
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(payload);    //Print request response payload
 
  http.end();  // Close connection
}

